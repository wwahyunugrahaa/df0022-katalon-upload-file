import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import org.openqa.selenium.interactions.Actions as Actions

// Open browser and navigate to The Internet website
WebUI.openBrowser('')

// Navigate to The Internet website
WebUI.navigateToUrl('https://the-internet.herokuapp.com/upload')

// Set file path
String filePath = 'C:\\Users\\USER\\Downloads\\18101152630068-removebg.png'

// Find drag and drop area
WebElement dragAndDropArea = WebUI.findWebElement(findTestObject('Element_Drag Drop Upload Area'))

// Create Actions object
Actions actions = new Actions(DriverFactory.getWebDriver())

// Perform drag and drop action
actions.clickAndHold(dragAndDropArea).moveToElement(DriverFactory.getWebDriver().findElement(findTestObject('Element_DragFile'))).release().build().perform()

// Upload file
WebUI.uploadFile(findTestObject('Element_Drag Drop Upload Area'), filePath)

// Click Upload button
WebUI.click(findTestObject('Element_DragFile'))

// Verify successful upload
WebUI.verifyElementVisible(findTestObject('Element_Input_Choose File'))

// Close the browser
WebUI.closeBrowser()

